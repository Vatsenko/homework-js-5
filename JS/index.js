'use strict'
//1.Спочатку ми пишемо function — це ключове слово (keyword), яке дає зрозуміти комп’ютеру, що далі буде оголошення функції.
// Потім — назву функції та список її параметрів в дужках (розділені комою).
// Якщо параметрів немає, ми залишаємо пусті дужки.
// І нарешті, код функції, який також називають тілом функції між фігурними дужками.

//2.return оператор який повертає значення з функції.
// За допомогою оператора return повертається результат виконання функції.
// Якщо у функції оператор return не використовується тоді функція повертає undefined.

//3.Параметр – це змінна між дужками функції (використовується під час оголошення функції)
// Аргумент – це значення, передане в функцію під час її виклику (використовується під час виконання функції).

//4. У JavaScript функції можуть передаватися як аргументи іншим функціям. Це дозволяє створювати функції вищого порядку.
// function greet(name) {
// console.log("Hello, " + name + "!");
// }
// function processUserInput(callback) {
// let name = prompt("Please enter your name:");
// callback(name);
// }
// // Передаємо функцію greet як аргумент функції processUserInput
// processUserInput(greet);
// В цьому прикладі функція greet передається як аргумент функції processUserInput.
// Коли processUserInput викликається, вона отримує greet як параметр callback і викликає її з переданим аргументом name.

//1.
function calculateQuotient(num1, num2) {
    if (num2 === 0) {
        console.log('Помилка: Ділення на нуль неможливе');
        return;
    }
    let quotient = num1 / num2;
    console.log(`Частка ${num1} і ${num2} дорівнює ${quotient}`);
    return quotient;
}

// Виклик функції з прикладом чисел
calculateQuotient(10, 2);

//2.
function validateNumber(input) {
    while (isNaN(input)) {
        input = prompt("Будь ласка, введіть число");
    }
    return Number(input);
}

function validateOperation(input) {
    while (!['+', '-', '*', '/'].includes(input)) {
        input = prompt("Будь ласка, введіть дію (+, -, *, /)");
    }
    return input;
}

function performOperation(num1, num2, operation) {
    switch (operation) {
        case '+':
            return num1 + num2;
        case '-':
            return num1 - num2;
        case '*':
            return num1 * num2;
        case '/':
            if (num2 === 0) {
                alert('Помилка: Ділення на нуль неможливе');
                return;
            }
            return num1 / num2;
        default:
            alert('Такої операції не існує');
            return;
    }
}

let num1 = validateNumber(prompt("Введіть перше число"));
let num2 = validateNumber(prompt("Введіть друге число"));
let operation = validateOperation(prompt("Введіть математичну операцію (+, -, *, /)"));

let result = performOperation(num1, num2, operation);
console.log(`Результат ${num1} ${operation} ${num2} = ${result}`);
//3.
function validNumber(input) {
    while (isNaN(input)) {
        input = prompt("Будь ласка, введіть число");
    }
    return Number(input);
}

function calculateFactorial(num) {
    let factorial = 1;
    for (let i = 2; i <= num; i++) {
        factorial *= i;
    }
    return factorial;
}

let num = validNumber(prompt("Введіть число для обчислення факторіалу"));
let factorial = calculateFactorial(num);
alert(`Факторіал числа ${num} дорівнює ${factorial}`);

